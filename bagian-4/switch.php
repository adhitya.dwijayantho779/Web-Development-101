<?php

$lunch = "burger";

switch ($lunch) {
    case "cereal":
        echo "Cereal? You had breakfast for lunch!";
        break;
    case "salad":
        echo "Salad makes for a healthy lunch!";
        break;
    case "burger":
        echo "Burgers are yummy!";
        break;
    default:
        echo "looks like you skipped lunch. That is not good";
}
