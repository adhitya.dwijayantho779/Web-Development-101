<?php

$x = 15;

if ($x < 10) {
    echo "$x is less than 10!";
} else if ($x > 20) {
    echo "$x is greater than 20!";
} else {
    echo "$x is between 10 and 20!";
}

?>